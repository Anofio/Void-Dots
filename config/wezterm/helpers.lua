Pallete = require 'colors'

local M = {}
local palette = Pallete

local active_tab = {
  bg_color = palette.bg2,
  fg_color = palette.fg0,
}

local inactive_tab = {
  bg_color = palette.bg2,
  fg_color = palette.fg2,
}

function M.colors()
  return {
    foreground = palette.fg2,
    background = palette.bg0,
    cursor_bg = palette.fg2,
    cursor_border = palette.fg2,
    cursor_fg = palette.fg0,
    selection_bg = palette.bg2,
    selection_fg = palette.fg0,

    ansi = {
      palette.fg2,
      palette.red,
      palette.green,
      palette.yellow,
      palette.blue,
      palette.purple,
      palette.orange,
      palette.fg1,
    },

    brights = {
      palette.fg1,
      palette.red,
      palette.green,
      palette.yellow,
      palette.blue,
      palette.purple,
      palette.orange,
      palette.fg0,
    },

    tab_bar = {
      background = palette.bg2,
      active_tab = active_tab,
      inactive_tab = inactive_tab,
      inactive_tab_hover = active_tab,
      new_tab = inactive_tab,
      new_tab_hover = active_tab,
      inactive_tab_edge = palette.fg1,
    },
  }
end

function M.window_frame()
  return {
    active_titlebar_bg = palette.bg2,
    inactive_titlebar_bg = palette.bg2,
  }
end

return M
