local os = require("os")
local wezterm = require("wezterm")

local act = wezterm.action

local leader_key = { key = "a", mods = "CTRL", timeout_miliseconds = 1000 }
    or {
      key = "Space",
      mods = "CTRL|SHIFT",
      timeout_miliseconds = 1000,
    }

local read_pywal_colors = function()
  -- read pywal colors if ~./cache/wal/colors exists and has 16 lines
  local pywal_colors = {}
  local pywal_cache = os.getenv("HOME") .. "/.cache/wal"

  -- check if wezterm.toml if exists load that as color_scheme
  -- TODO fix template
  -- local toml_colors_file = io.open(pywal_cache .. "/wezterm-wal.toml", "r")
  -- if toml_colors_file then
  -- 	toml_colors_file:close()
  -- 	wezterm.add_to_config_reload_watch_list(pywal_cache .. "/wezterm-wal.toml")
  --
  -- 	return {
  -- 		color_scheme_dirs = { pywal_cache },
  -- 		-- color_scheme = "wezterm-wal",
  -- 	}
  -- end

  local pywal_cache_file = io.open(pywal_cache .. "/colors", "r")
  local err = nil

  if pywal_cache_file then
    local pyfile_c = pywal_cache_file:read("*a")
    local color_lines = wezterm.split_by_newlines(pyfile_c)
    local line_count = color_lines and #color_lines or 0 -- means 0 if color_lines is nil
    pywal_cache_file:close()
    if line_count == 16 then
      pywal_colors = {
        foreground = color_lines[16],
        background = color_lines[1],

        cursor_bg = color_lines[8],
        cursor_fg = color_lines[8],
        cursor_border = color_lines[8],

        selection_fg = color_lines[8],
        selection_bg = "#2d4f67",

        scrollbar_thumb = "#16161d",
        split = "#16161d",

        ansi = { table.unpack(color_lines, 1, 8) },

        brights = { table.unpack(color_lines, 9, 16) },
      }
    elseif line_count == 18 then
      pywal_colors = {
        foreground = color_lines[16],
        background = color_lines[1],

        cursor_bg = color_lines[8],
        cursor_fg = color_lines[8],
        cursor_border = color_lines[8],

        selection_fg = color_lines[8],
        selection_bg = "#2d4f67",

        scrollbar_thumb = "#16161d",
        split = "#16161d",

        ansi = { table.unpack(color_lines, 1, 8) },

        brights = { table.unpack(color_lines, 9, 16) },
        indexed = { [16] = color_lines[17], [17] = color_lines[18] },
      }
    else
      err = string.format("pywal colors file has wrong number of lines %d", line_count)
    end

    -- pywal_cache_file:close()
  else
    err = "pywal colors file not found"
  end

  if err then
    wezterm.log_error(string.format("error reading pywal colors: %s", err))
    return {}
  end
  wezterm.log_info("successfully loaded pywal colors using raw method")
  return {
    colors = pywal_colors,
  }
end

local getDevProfile = function()
  wezterm.log_info("getDevProfile called")

  local profiles = {
    {
      label = "pwsh-dev",
      -- args = { "pwsh", "-NoLogo", "-NoExit", "-Command", "cd $env:DEV_HOME; clear;" },
      args = {
        "powershell.exe",
        '-NoExit -Command "&{Import-Module """C:\\Program Files (x86)\\Microsoft Visual Studio\\2022\\BuildTools\\Common7\\Tools\\Microsoft.VisualStudio.DevShell.dll"""; Enter-VsDevShell f5dab38b -SkipAutomaticLocation -DevCmdArguments """-arch=x64 -host_arch=x64"""}"',
      },
    },
    {
      label = "cmd-dev",
      args = {
        "cmd",
        "/k",
        "C:\\Program Files (x86)\\Microsoft Visual Studio\\2022\\BuildTools\\Common7\\Tools\\VsDevCmd.bat",
      },
    },
  }

  return profiles
end


local pywal_colors = read_pywal_colors()
if not pywal_colors then
  pywal_colors = {
    colors = nil,
    color_scheme = nil,
    color_scheme_dirs = wezterm.color.color_scheme_dirs,
  }
end

return {
  font = wezterm.font("JetBrains Nerd Mono", { weight = "Medium" }),

  -- color_scheme='Wryan',
  colors = pywal_colors.colors,
  color_scheme = pywal_colors.color_scheme,
  color_scheme_dirs = pywal_colors.color_scheme_dirs,
  default_cwd = os.getenv("HOME"),
  window_background_opacity = 0.8,
  text_background_opacity = 0.98,
  default_cursor_style = "SteadyBar",

  window_decorations = "RESIZE",
  hide_tab_bar_if_only_one_tab = true,
  leader = leader_key,
  keys = {
    -- {key ='m',mods='CMD',action='DisableDefaultAssignment'},
    { key = "l", mods = "ALT",    action = act.ShowLauncher },
    { key = "-", mods = "LEADER", action = act.SplitVertical({ domain = "CurrentPaneDomain" }) },
    { key = "|", mods = "LEADER", action = act.SplitHorizontal({ domain = "CurrentPaneDomain" }) },
    {
      key = "UpArrow",
      mods = "SHIFT",
      action = act.ScrollByLine(-1),
    },
    {
      key = "DownArrow",
      mods = "SHIFT",
      action = act.ScrollByLine(1),
    },
    {
      key = "r",
      mods = "LEADER",
      action = act.ActivateKeyTable({
        name = "resize_pane",
        one_shot = false,
        replace_current = true,
      }),
    },
    {
      key = "a",
      mods = "LEADER",
      action = act.ActivateKeyTable({
        one_shot = false,
        name = "activate_pane",
        timeout_miliseconds = 1000,
        replace_current = false,
      }),
    },
  },
  key_tables = {
    -- Defines the keys that are active in our resize-pane mode.
    -- Since we're likely to want to make multiple adjustments,
    -- we made the activation one_shot=false. We therefore need
    -- to define a key assignment for getting out of this mode.
    -- 'resize_pane' here corresponds to the name="resize_pane" in
    -- the key assignments above.
    resize_pane = {
      { key = "LeftArrow",  action = act.AdjustPaneSize({ "Left", 1 }) },
      { key = "h",          action = act.AdjustPaneSize({ "Left", 1 }) },
      { key = "RightArrow", action = act.AdjustPaneSize({ "Right", 1 }) },
      { key = "l",          action = act.AdjustPaneSize({ "Right", 1 }) },
      { key = "UpArrow",    action = act.AdjustPaneSize({ "Up", 1 }) },
      { key = "k",          action = act.AdjustPaneSize({ "Up", 1 }) },
      { key = "DownArrow",  action = act.AdjustPaneSize({ "Down", 1 }) },
      { key = "j",          action = act.AdjustPaneSize({ "Down", 1 }) },

      -- Cancel the mode by pressing escape
      { key = "Escape",     action = "PopKeyTable" },
    },

    -- Defines the keys that are active in our activate-pane mode.
    -- 'activate_pane' here corresponds to the name="activate_pane" in
    -- the key assignments above.
    activate_pane = {
      { key = "LeftArrow",  action = act.ActivatePaneDirection("Left") },
      { key = "h",          action = act.ActivatePaneDirection("Left") },
      { key = "RightArrow", action = act.ActivatePaneDirection("Right") },
      { key = "l",          action = act.ActivatePaneDirection("Right") },
      { key = "UpArrow",    action = act.ActivatePaneDirection("Up") },
      { key = "k",          action = act.ActivatePaneDirection("Up") },
      { key = "DownArrow",  action = act.ActivatePaneDirection("Down") },
      { key = "j",          action = act.ActivatePaneDirection("Down") },
      { key = "Escape",     action = "PopKeyTable" },
    },
  },
}
