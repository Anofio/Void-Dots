#!/usr/bin/env bash

battery_level=$(cat /sys/class/power_supply/BAT1/capacity)
if [ $battery_level -lt 15 ]; then
  notify-send "Battery low" "Plug in charger"
fi
