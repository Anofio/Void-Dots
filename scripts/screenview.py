#!/usr/bin/env python
from pytesseract import pytesseract
from sys import argv


class ScreenViewer:
    def __init__(self, img_path: str, lang: str = "eng") -> None:
        self.tesseract = pytesseract
        self.tesseract.tesseract_cmd = "/usr/bin/tesseract-ocr"
        self.img: str = img_path
        self.lang: str = lang

    def process_image(self) -> str:
        return pytesseract.image_to_string(self.img, lang=self.lang)


def main():
    lang: str = "eng"
    image: str = argv[1]
    try:
        lang = argv[2]
    except IndexError:
        pass
    screenviewer = ScreenViewer(image, lang)
    print(screenviewer.process_image())


if __name__ == "__main__":
    main()
