#!/usr/bin/env bash

if pgrep -x "swww-daemon" >/dev/null; then
  (swww img "$UNITHEME_DIR/wallpapers/current/wallpaper.png" &);
else
  swww-daemon &
  while (pgrep -u "$(id -u)" -x swww-deamon >/dev/null);
    do
      sleep 0.5;
    done;
  swww img "$UNITHEME_DIR/wallpapers/current/wallpaper.png" &
fi
