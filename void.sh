#/bin/bash

echo "Installing opendoas..."
echo "su -c xbps-install opendoas"
su -c "xbps-install opendoas"
echo "su -c echo 'permit persist :wheel' > /etc/doas.conf"
su -c "echo 'permit persist :wheel' > /etc/doas.conf"

pkgs="elogind dbus zsh git curl wget brillo ccache mesa-dri linux-firmware-amd vulkan-loader mesa-vulkan-radeon xf86-video-amdgpu mesa-vaapi mesa-vdpau ffmpeg tlp wlroots btop htop neovim firefox Waybar alacritty telegram-desktop alsa-utils pipewire wireplumber bluez libspa-bluetooth xdg-desktop-portal xdg-utils xdg-user-dirs noto-fonts-ttf noto-fonts-cjk noto-fonts-emoji python3-pip python3-requests pfetch chrony fzf libnotify pnpm wl-clipboard slurp jq grim qt5ct kvantum 7zip"


# Install pkgs
echo "Installing packages..."
doas xbps-install -Su ${pkgs}

# Clone void-packages
echo "Cloning void-packages..."
git clone https://github.com/void-linux/void-packages $HOME/void-packages
echo "binary-bootstrap..."
$HOME/void-packages/xbps-src binary-bootstrap

# Custom repo
echo "Installing Custom Repo..."
git clone https://codeberg.org/Anofio/VoidRepo.git $HOME/Documents/Git/Void-Dots/VoidRepo
7z x $HOME/Documents/Git/Void-Dots/VoidRepo/repo.7z -o$HOME/Documents/Git/Void-Dots/VoidRepo/repo
mkdir -p $HOME/void-packages/hostdir/binpkgs
mv $HOME/Documents/Git/Void-Dots/Void-Dots/repo/* $HOME/void-packages/hostdir/binpkgs

# Install Hyprland
echo "Installing hyprland..."
doas xbps-install -R $HOME/void-packages/hostdir/binpkgs hyprland

# Enable services
echo "Enabling services..."
doas ln -s /etc/sv/bluetoothd /var/service
doas ln -s /etc/sv/chronyd /var/service

# Linking pipewire modules
echo "Linking pipewire modules..."
doas ln -s /usr/share/examples/wireplumber/10-wireplumber.conf /etc/pipewire/pipewire.conf.d
doas ln -s /usr/share/examples/pipewire/20-pipewire-pulse.conf /etc/pipewire/pipewire.conf.d

# Configs
echo "Installing configs..."
mkdir -p $HOME/.config
cp -r $HOME/Documents/Git/Void-Dots/config/* $HOME/.config/

# Themes
echo "Installing themes..."
git clone https://codeberg.org/Anofio/unitheme.git $HOME/.config/themes

# Scritps
echo "Installing scripts..."
mkdir -p $HOME/.local/share/scripts
cp -r $HOME/Documents/Git/Void-Dots/scripts/* $HOME/.local/share/scripts/

# Fonts
echo "Installing fonts..."
mkdir -p $HOME/.local/share/fonts
cp -r $HOME/Documents/Git/Void-Dots/fonts/* $HOME/.local/share/fonts
doas rm /etc/fonts/conf.d/10-sub-pixel-none.conf
doas ln -s /usr/share/fontconfig/conf.avail/10-sub-pixel-rgb.conf /etc/fonts/conf.d
doas ln -s /usr/share/fontconfig/conf.avail/09-autohint-if-no-hinting.conf /etc/fonts/conf.d
fc-cache -fv

# Rofi
echo "Installing lbonn-rofi-wayland-void"
doas xbps-install -R $HOME/void-packages/hostdir/binpkgs lbonn-rofi-wayland lbonn-rofi-wayland-emojii

# Zsh
echo "Installing Zsh..."
git clone https://codeberg.org/Anofio/zsh-p10k-void.git $HOME/Documents/Git/zsh-p10k-void
cp -r $HOME/Documents/Git/zsh-p10k-void/zsh-p10k $HOME/void-packages/srcpkgs
$HOME/void-packages/xbps-src pkg zsh-p10k
doas xbps-install -R $HOME/void-packages/hostdir/binpkgs zsh-p10k

echo "Installing Zap fot Zsh..."
zsh <(curl -s https://raw.githubusercontent.com/zap-zsh/zap/master/install.zsh) --branch release-v1
cp $HOME/Documents/Git/Void-Dots/zsh/.zshrc $HOME
cp $HOME/Documents/Git/Void-Dots/zsh/.zshenv $HOME
zsh

echo "Installing amdgpu_top..."
doas xbps-install -R $HOME/void-packages/hostdir/binpkgs amdgpu_top

# 10-ignore.conf 
echo "Xbps ignore packages"
doas cp -r $HOME/Documents/Git/Void-Dots/10-ignore.conf /etc/xbps.d/
doas xbps-remove -Ro linux-firmware-nvidia linux-firmware-broadcom linux-firmware-intel

# Reboot
echo "Everything is ready"
echo "Rebooting..."
doas reboot
