# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if test -z "${XDG_RUNTIME_DIR}"; then
  export XDG_RUNTIME_DIR=/tmp/${UID}-runtime-dir
  if ! test -d "${XDG_RUNTIME_DIR}"; then
    mkdir "${XDG_RUNTIME_DIR}"
    chmod 0700 "${XDG_RUNTIME_DIR}"
  fi
fi
if [ -z "$DISPLAY" ] && [ "$(fgconsole)" -eq 1 ]; then
  WLR_RENDERER=vulkan dbus-run-session Hyprland
fi

# Created by Zap installer
[ -f "${XDG_DATA_HOME:-$HOME/.local/share}/zap/zap.zsh" ] && source "${XDG_DATA_HOME:-$HOME/.local/share}/zap/zap.zsh"
plug "zsh-users/zsh-autosuggestions"
plug "zsh-users/zsh-syntax-highlighting"
plug "zap-zsh/supercharge"
plug "zap-zsh/zap-prompt"
# plug "sadiksaifi/zsh-dynamic-prompt"
plug "zap-zsh/fzf"
# plug "romkatv/powerlevel10k"
# plug "MAHcodes/distro-prompt"
plug "Aloxaf/fzf-tab"
# plug "kutsan/zsh-system-clipboard"
# plug "Freed-Wu/fzf-tab-source"
plug "wintermi/zsh-lsd"

# Load and initialise completion system
autoload -Uz compinit
compinit

alias in="doas pacman -S"
alias re="doas pacman -Rns"
alias recl="doas pacman -R"
alias up="doas pacman -Syu"
alias clean="pacman -Qtdq"
alias ed="nvim"
alias t="touch"
alias m="mkdir -p"
alias pwine="env $HOME/PortWINE/PortProton/data/scripts/start.sh %F"
alias red="doas nvim"
alias reboot="doas reboot"
alias halt="doas shutdown -h now"
alias fonts="fc-list :lang=hi"
alias sv="doas sv"
alias sex="echo \"no sex :(\""

xf ()
{
  if [ -f $1 ] ; then
    case $1 in
      *.tar.bz2)   tar xjf $1   ;;
      *.tar.gz)    tar xzf $1   ;;
      *.bz2)       bunzip2 $1   ;;
      *.rar)       unrar x $1   ;;
      *.gz)        gunzip $1    ;;
      *.tar)       tar xf $1    ;;
      *.tbz2)      tar xjf $1   ;;
      *.tgz)       tar xzf $1   ;;
      *.zip)       unzip $1     ;;
      *.Z)         uncompress $1;;
      *.7z)        7z x $1      ;;
      *.deb)       ar x $1      ;;
      *.tar.xz)    tar xf $1    ;;
      *.tar.zst)   tar xf $1    ;;
      *)           echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}

bindkey "^[[1;5C" forward-word
bindkey "^[[1;5D" backward-word

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
